import React from "react";
import { Switch ,Link,Route } from "react-router-dom";
import logo from './public/img/logo.png'
import './public/css/style.css'

import Index from '../Quiz3/index';
import About from '../Quiz3/about';
import Editor from '../Quiz3/editor';
import Login from '../Quiz3/login';

const Routes = () => {

    return (
    <>
      <header>
        <img id="logo" src={logo} style={{width:"200px"}} />
        <nav>
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/editor">Movie List Editor</Link></li>
            <li><Link to="/login">Login</Link></li>
          </ul>
        </nav>
      </header>
      
    <Switch>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/editor">
        <Editor />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/">
        <Index />
      </Route> 
    </Switch>
    <footer style={{bottom:"0", position:"relative"}}>
        <h5>copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </>
    )
}

export default Routes;