import React from "react"
import {EditorProvider} from "./editorContext"
import EditorList from "./editorList"
import EditorForm from "./editorForm"
import './public/css/editor.css';

const Editor = () => {
  return(
    <EditorProvider>
      <div style={{backgroundColor: "white", width: "50%", margin: "80px auto", padding: "20px 40px", display: "block"}}>
        <div style={{padding: "20px"}}>
        <EditorList/>
        <EditorForm/>
        </div>
      </div>
    </EditorProvider>
  )
}
export default Editor