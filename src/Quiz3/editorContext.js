import React, { useState,  createContext } from "react";

export const EditorContext = createContext();

export const EditorProvider = props => {
    const [movie, setMovie] =  useState(null)
    const [input, setInput]  =  useState({title: "", description: "", year: 0, duration: 0, genre: "", rating: 0})
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    return (
        <EditorContext.Provider value={[movie,setMovie,input, setInput,selectedId, setSelectedId,statusForm, setStatusForm]}>
            {props.children}
        </EditorContext.Provider>
    )
}
