import React, {useContext,useEffect} from "react"
import {EditorContext} from "./editorContext"
import axios from "axios"

const EditorForm = () =>{
    const [movie,setMovie,input, setInput,selectedId, setSelectedId,statusForm, setStatusForm] = useContext(EditorContext)

    useEffect( () => {
        if (movie === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setMovie(res.data.map(el=>{ return {id: el.id, title: el.title, description: el.description, year: el.year, duration: el.duration,genre: el.genre, rating: el.rating}} ))
          })
        }
      }, [movie])
    
      const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "title":
          {
            setInput({...input, title: event.target.value});
            break
          }
          case "rating":
          {
            setInput({...input, rating: event.target.value});
            break
          }
          case "description":
          {
            setInput({...input, description: event.target.value});
              break
          }
          case "year":
          {
            setInput({...input, year: event.target.value});
              break
          }
          case "genre":
          {
            setInput({...input, genre: event.target.value});
              break
          }
          case "duration":
          {
            setInput({...input, duration: event.target.value});
              break
          }
          default:
            {break;}
        }
      }
    
      const handleSubmit = (event) =>{
        // menahan submit
        event.preventDefault()
    
        let title = input.title
        let genre = input.genre
        let rating = input.rating.toString()
        let duration = input.duration.toString()
        let year = input.year.toString()
        let description = input.description
        
    
        if (title.replace(/\s/g,'') !== "" && genre.replace(/\s/g,'') !== "" && rating.replace(/\s/g,'') !== "" && duration.replace(/\s/g,'') !== "" && year.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== ""){      
          if (statusForm === "create"){        
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title, description: input.description, year: input.year, duration: input.duration,genre: input.genre, rating: input.rating})
            .then(res => {
                setMovie([
                  ...movie, 
                  { id: res.data.id, 
                    title : input.name, 
                    genre : input.genre,
                    rating : input.rating,
                    duration : input.duration,
                    year : input.year,
                    description : input.description
                  }])
            })
          }else if(statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title: input.title, description: input.description, year: input.year, duration: input.duration,genre: input.genre, rating: input.rating})
            .then(() => {
                let dataMovie = movie.find(el=> el.id === selectedId)
                  dataMovie.title = input.name 
                  dataMovie.genre = input.genre
                  dataMovie.rating = input.rating
                  dataMovie.duration = input.duration
                  dataMovie.year = input.year
                  dataMovie.description = input.description
                setMovie([...movie])
            })
          }
          
          setStatusForm("create")
          setSelectedId(0)
          setInput({title: "", description: "", year: 0, duration: 0, genre: "", rating: 0})
        }
      }

    return(
      <>
        <h1>Form Movie</h1>
        <p>*Setelah Edit dan Submit, Refresh. Saya ngga tau mas ini kenapa ngga langsung nongol. Maaf mas agak berantakan</p>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              Title
            </label>
            <input style={{width :"150px"}} type="text" name="title" value={input.title} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{width :"150px",float: "left",textAlign:"left"}}>
              Rating 
            </label>
            <input style={{width :"150px"}} type="number" min="1" max="10" name="rating" value={input.rating} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{width :"150px",float: "left",textAlign:"left"}}>
              Duration
            </label>
            <input style={{width :"150px"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{width :"150px",float: "left",textAlign:"left"}}>
              Year
            </label>
            <input style={{width :"150px"}} type="number" name="year" value={input.year} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{width :"150px",float: "left",textAlign:"left"}}>
              Genre
            </label>
            <input style={{width :"150px"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{width :"150px",float: "left",textAlign:"left"}}>
              Description
            </label>
            <input style={{width : "150px",height : "80px"}} type="text" name="description" value={input.description} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "left"}}>submit</button>
            </div>
          </form>
      </>
    )
}

export default EditorForm