import React, {useContext,useEffect} from "react"
import {EditorContext} from "./editorContext"
import axios from "axios"

const EditorList = () =>{
    const [movie,setMovie,input, setInput,selectedId, setSelectedId,statusForm, setStatusForm] = useContext(EditorContext)
    
    const handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)
    
        let newMovie = movie.filter(el => el.id !== idMovie)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
        .then(res => {
          console.log(res)
        })
              
        setMovie([...newMovie])
        
      }
      
      const handleEdit = (event) =>{
        let idMovie = parseInt(event.target.value)
        let dataMovie = movie.find(x=> x.id === idMovie)
        setInput({title: dataMovie.title, description: dataMovie.description, year: dataMovie.year, duration: dataMovie.duration,genre: dataMovie.genre, rating: dataMovie.rating})
        setSelectedId(idMovie)
        setStatusForm("edit")
      }

    return(
        <>
        <h1>Movie List</h1>
        <table>
        <thead>
          <tr>
            <th>No</th>
            <th style={{width: "60%"}}>Nama</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            {
              movie !== null && movie.map((item, index)=>{
                return(                    
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>
      </>
    )
}

export default EditorList