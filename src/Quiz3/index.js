import React from "react"
import {IndexProvider} from "./indexContext"
import IndexList from "./indexList"

import './public/css/style.css'

const Index = () =>{
  return(
    <IndexProvider>
      <IndexList/>
    </IndexProvider>
  )
}

export default Index;