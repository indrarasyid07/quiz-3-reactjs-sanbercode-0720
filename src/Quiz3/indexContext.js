import React, { useState,  createContext } from "react";

export const IndexContext = createContext();

export const IndexProvider = props => {
    const [movie, setMovie] =  useState(null)

    return (
        <IndexContext.Provider value={[movie,setMovie]}>
            {props.children}
        </IndexContext.Provider>
    )
}

