import React, {useContext,useEffect} from "react"
import {IndexContext} from "./indexContext"
import axios from "axios"

const IndexList = () =>{
    const [movie,setMovie] = useContext(IndexContext)
    useEffect( () => {
        if (movie === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setMovie(res.data.map(el=>{ return {id: el.id, title: el.title, description: el.description, year: el.year, duration: el.duration,genre: el.genre, rating: el.rating}}))
          })
        }
      }, [movie])
    
      return(
        <body>
        <section>
        <h1>Daftar Film Terbaik</h1>
          {
          movie !== null && movie.map((item, index)=>{
            return(
    
          <div id="article-list">
            <div>
              <a href=""><h3>{item.title}</h3></a>
              <p>
              <strong>Rating : </strong>{item.rating}
              </p>
              <p>
              <strong>Duration : </strong>{item.duration*60} minute
              </p>
              <p>
              <strong>Year : </strong>{item.year}
              </p>
              <p>
              <strong>Genre : </strong>{item.genre}
              </p>
              <p>
              <strong>Description : </strong>{item.description}
              </p>
            </div>
          </div>
            )}).sort((a,b) => {return a - b})}
        </section>
        </body>
      )
}

export default IndexList